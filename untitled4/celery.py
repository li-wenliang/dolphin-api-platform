# -*- coding:utf-8 -*-
from __future__ import absolute_import
import logging

logger = logging.getLogger('log')
import os
from celery import Celery, platforms
from kombu import Exchange
from kombu import Queue
import datetime
from django.utils import timezone
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'untitled4.settings')

from django.conf import settings  # noqa

app = Celery('untitled4')
# app.now = timezone.now
app.now = datetime.datetime.utcnow
# # 解决时区问题,定时任务启动就循环输
# app.now = datetime.datetime.now


# 用来发现放到 django settings 中的 celery 配置
app.config_from_object('django.conf:settings')


# 用来发现放到django app中的task
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# 允许root 用户运行celery
platforms.C_FORCE_ROOT = True


# 设置了三个Queue绑定到一个direct类型的exchange上,然后consumer监听所有的队列,消息来了后就轮询调用consumer进行处理.
task_exchange = Exchange('tasks', type='direct')
# 异步任务优先级
task_queues = [Queue('hipri', task_exchange, routing_key='hipri'),
               Queue('midpri', task_exchange, routing_key='midpri'),
               Queue('lopri', task_exchange, routing_key='lopri')]
# from celery.schedules import crontab
#
# from datetime import timedelta


# app.conf.update(  # 定时任务
#     CELERYBEAT_SCHEDULE={
#         'sum-task': {
#             'task': 'myblog.tasks.specifyScheduledTasks',
#             'schedule': timedelta(minutes=10),  # 每10分组S执行一次
#             # 'args': (5, 6),传参
#             # days：天
#             # seconds：秒schedule
#             # microseconds：微妙
#             # milliseconds：毫秒
#             # minutes：分
#             # hours：小时
#         },
#         # 'send-report': {
#         #     'task': 'myblog.tasks.add',
#         #     'schedule': crontab(hour=4, minute=30, day_of_week=1),#每周一早上4：30执行report函数
#         #     # crontab的参数有：
#         #     # month_of_year：月份
#         #     # day_of_month：日期
#         #     # day_of_week：周
#         #     # hour：小时
#         #     # minute：分钟
#         # }
#     }
# )
# app = Celery('website', backend='redis', broker='redis://localhost')

@app.task(bind=True)
def debug_task(self):
    # print('Request: {0!r}'.format(self.request))
    logger.info('Request: {0!r}'.format(self.request))
