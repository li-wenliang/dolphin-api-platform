from myblog.models import DolphinUser
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication

class Authentication(BaseAuthentication):

    '''
    自定义的用户认证类
    '''
    def authenticate(self,request):
        # 首先验证用户传递的token值和数据库中的是否一致
        token = request.META.get('HTTP_TOKEN')
        uuid=request.META.get('HTTP_UUID')
        if not token:
            raise exceptions.AuthenticationFailed('认证失败')
        else:
            token_obj = DolphinUser.objects.filter(token=token,user_id=uuid)
            if not token_obj:
                raise exceptions.AuthenticationFailed('token认证失败')
            else:
                return (token_obj[0],token_obj[0].token)
