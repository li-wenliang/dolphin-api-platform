# Generated by Django 2.0.2 on 2020-04-10 11:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('myblog', '0005_auto_20200410_1106'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dolphinresponsecontentreport',
            name='plan_id',
            field=models.AutoField(db_index=True, max_length=200, primary_key=True, serialize=False),
        ),
    ]
