# Generated by Django 2.0.2 on 2020-04-10 11:23

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('myblog', '0006_auto_20200410_1109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dolphinresponsereportresult',
            name='plan_id',
            field=models.CharField(db_index=True, default='', max_length=200),
        ),
    ]
