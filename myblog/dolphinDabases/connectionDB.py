from django.db import connection
import  logging
logger = logging.getLogger('log')
def getSql(sql):
    try:
        # 获取游标
        cursor = connection.cursor()
        # 查询数据
        cursor.execute(sql)  # 执行sql
        connection.commit()
        if cursor.rowcount == 0:
            logger.info("当前查询的SQL查询内容为空：%s"%(sql))
            return 0
        else:
            data = cursor.fetchall()
            for hrow in data:
                logger.info("当前sql:%s,查询出的内容是：%s"%(sql,hrow))
                return hrow
    except  Exception as e:
        logger.error(e)
    finally:
        cursor.close()
        connection.close()


def getmoreSql(sql):
    try:
        cursor = connection.cursor()  # 获得一个游标(cursor)对象
        cursor.execute(sql)
        rawData = cursor.fetchall()
        col_names = [desc[0] for desc in cursor.description]
        result = []
        for row in rawData:
            objDict = {}
            # 把每一行的数据遍历出来放到Dict中
            for index, value in enumerate(row):
                objDict[col_names[index]] = value
            result.append(objDict)
        return result

    except  Exception as e:
        logger.error(e)
    finally:
        cursor.close()
        connection.close()
