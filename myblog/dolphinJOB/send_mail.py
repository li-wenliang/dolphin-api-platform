#!/usr/bin/env python
import smtplib, logging
from email.header import Header
from email.mime.text import MIMEText
from myblog.dolphinJOB import sendMailHtml
from myblog.models import DolphinTestEmail
logger = logging.getLogger('log')
def send_email1(data):
    emaillist = DolphinTestEmail.objects.get()
    host = emaillist.host  # SMTP服务器
    port = int(emaillist.port)
    user = emaillist.user  # 用户名
    passWD = emaillist.passWD  # 授权密码，非登录密码
    sender = emaillist.sender  # 发邮件人
    receivers = eval(emaillist.receivers)  # 收邮件人
    subject = '测试自动化平台测试报告'  # 邮件主题
    content = sendMailHtml.sandHtml(data)
    meg = MIMEText(content + ' \n cuihaiqi:13067750910', _subtype='html', _charset='utf-8')  # 内容, 格式, 编码
    meg['From'] = user  # 这两种方法都一样的
    meg['From'] = "{}".format(user)
    meg['To'] = ','.join(receivers)
    meg['Subject'] = subject + '( 自动发送邮件切勿回复)'
    try:
        smtpObj = smtplib.SMTP_SSL(host, port)  # 启用SSL发信, 端口一般是465
        smtpObj.login(user, passWD)  # 登录验证
        smtpObj.sendmail(sender, receivers, meg.as_string())  # 发送
        logger.info("---邮件已发送---")
    except smtplib.SMTPException as e:
        logger.error("---邮件发送失败---")
        logger.error(e)
