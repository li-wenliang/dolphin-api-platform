import requests
import urllib3
import logging
logger = logging.getLogger('log')
import json
timeout = 7000
class ConfigHttp:
    urllib3.disable_warnings()

    def jsonget(self):
        """
        defined get method
        :return:
        """
        try:
            response = requests.get(self.url, headers=self.headers, params=self.params,
                                    timeout=float(timeout),
                                    verify=False, cookies=self.cookies)
            # response.raise_for_status()
            return response
        except TimeoutError as e:
            logger.error("Time out!", e)
            return None

    def jsonpost(self):

        """
        defined post method
        :return:
        """
        try:
            response = requests.post(url=self.url, headers=self.headers, json=self.data,
                                     timeout=float(timeout),
                                     verify=False, cookies=self.cookies)
            return response
        except TimeoutError as e:
            logger.error("Time out!", e)
            return None

    def dataget(self):
        """
        defined get method
        :return:
        """
        try:
            logger.info(self.headers)

            response = requests.get(self.url, headers=self.headers, params=self.params,
                                    timeout=float(timeout),
                                    verify=False)
            return response
        except TimeoutError as e:
            logger.error("Time out!", e)
            return None

    def datapost(self):
        """
        defined post method
        :return:
        """
        try:
            response = requests.post(url=self.url, headers=self.headers, data=self.data,
                                     timeout=float(timeout),
                                     verify=False, cookies=self.cookies, )
            return response
        except TimeoutError as e:
            logger.error("Time out!", e)
            return None
