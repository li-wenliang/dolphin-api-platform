from xml.etree import ElementTree as ElementTree
import os

propath = os.path.dirname(os.path.realpath(__file__))

# ****************************** read SQL xml ********************************
database = {}  # 默认配置为空


def set_xml(sqlname, fileName):  #
    """
    set sql xml
    :return:解析SQL.xml中内容
    """

    proDir = os.path.join(os.path.dirname(propath), 'dolphinService/%s') % (fileName)
    # 下面判断就是先考虑最外层解析的database,在解析中层的table 最后解析最底层的ID标识
    if len(database) >= 0:  # 如果数据集合为空时
        sql_path = os.path.join(proDir, '%s') % (sqlname)  # 打开配置文件地址
        tree = ElementTree.parse(sql_path)  # 解析配文件
        for db in tree.findall("database"):  # 全局解析XML中database属性
            db_name = db.get("name")  # 解析获得每一个database属性的table属性中的名称
            table = {}
            for tb in db.getchildren():  # 获得解析的database的子属性
                table_name = tb.get("name")  # 获得table子属性的标识名
                sql = {}
                for data in tb.getchildren():  # 获得table子属性的标识名
                    sql_id = data.get("id")  # 获得id子属性的标识名
                    sql[sql_id] = data.text  # 得到对应ID属性的内容
                table[table_name] = sql  # 得到对应table属性的内容
            database[db_name] = table  # 得到对应的的数据库中内容


def get_xml_dict(database_name, table_name, sqlname, fileName):
    """
    :param database_name: 获得SQL.xml的database的名称
    :param table_name:获得数据库名称对应的table的名称
    :param sqlname获得对应的SQLxml的名称
    :return:database_dict 返回SQL的集合
    """
    set_xml(sqlname, fileName)
    database_dict = database.get(database_name).get(table_name)
    return database_dict  # 获得当前datbase下table的所有SQL的集合


def get_sql(database_name, table_name, sql_id, sqlname, fileName):
    """
    获得指定的SQL语句

    :param database_name:get SQL.xml by given database's name
    :param table_name:get SQL.xml by given table's name
    :param sql_id:The SQL.xml for sql_index
    :param:sqlname :Get the name of the corresponding SQLxml
    :return:sql
    """

    db = get_xml_dict(database_name, table_name, sqlname, fileName)
    sql = db.get(sql_id)
    sql = sql.strip()

    return sql
