from django.db import models


# Create your models here.

class DolphinTestOperatingEnvironment(models.Model):  # 运行环境表
    oe_id = models.AutoField(primary_key=True, null=False, db_index=True)  # 运行环境id
    oe_text = models.TextField(default='')  # 运行环境内容
    creat_time = models.DateTimeField(auto_now_add=True)  # 当前数据的创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 当前数据的修改时间
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestProject(models.Model):  # 项目表
    pj_id = models.AutoField(primary_key=True, null=False, db_index=True)  # 项目id
    pj_name = models.TextField()  # 项目名称
    pj_img = models.TextField(default='')  # 项目图片地址
    pj_running_address = models.TextField(default='')  # 项目运行地址
    configure_address = models.TextField(default='')  # 微服务运行地址1
    otherconfigure = models.TextField(default='')  # 微服务运行地址2
    creat_time = models.DateTimeField(auto_now_add=True)  # 当前数据的创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 当前数据的修改时间
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestGrouping(models.Model):  # 分组表
    group_id = models.AutoField(primary_key=True, null=False, db_index=True)  # 分组id
    group_name = models.TextField()  # 分组名称
    group_pj_id = models.TextField()  # 关联项目ID
    creat_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 修改时间
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestRequest(models.Model):  # 接口表
    request_id = models.AutoField(primary_key=True, null=False, db_index=True)  # 接口id
    request_name = models.TextField()  # 接口名称
    request_group_id = models.TextField()  # 分组ID
    request_address = models.TextField()  # 接口地址
    request_mode = models.TextField()  # 请求接口方式
    creat_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 修改时间
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestRequestTestCase(models.Model):  # 测试用例
    case_id = models.AutoField(primary_key=True, null=False, db_index=True)  # 用例id
    case_name = models.TextField()  # 用例名称名称
    case_request_id = models.TextField()  # 关联接口ID
    request_header_param = models.TextField(default="")  # herder 请求接口中用到的头部信息
    headerData = models.TextField(default="")  # herder动态取值内容
    request_data_content = models.TextField(default="")  # 请求接口的参数
    request_other_type = models.TextField(default="")  # 是否使用关联接口
    request_relation_content = models.TextField(default="")  # 关联接口内容 1，存入接口地址,入参数据和提取数据字段
    request_response = models.TextField(default="")  # 响应内容
    request_code = models.TextField(default="")  # 断言结果
    request_result = models.TextField(default="")  # 入参动态 断言内容
    request_result_Assertion_content = models.TextField(default="")  # 结果断言内容
    creat_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 修改时间
    otherCaseid = models.TextField(default="")  # 关联其他用例id
    otherCaseidConclusionValue = models.TextField(default="")  # 关联其他取值
    dataType = models.TextField(default="")  # 接口请求类型JSON|type
    sqlContent = models.TextField(default="")  # 前后置接口内容
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestPublicInterface(models.Model):  # 存储公共接口
    request_id = models.AutoField(primary_key=True, null=False, db_index=True)  # primary_key:主键 ;自动增量;NUll:是否可以为空
    url = models.TextField()  # url 请求接口的地址
    method = models.CharField(max_length=10)  # 请求接口的方式  post get
    request_data_type = models.TextField(default='')  # 接口请求类型JSON|type
    request_header_param = models.TextField(default='')  # herder 请求接口中用到的头部信息
    request_data_content = models.TextField(default='')  # 请求接口的参数
    request_value = models.TextField(default='')  # 接口动态取值
    creat_time = models.DateTimeField(auto_now_add=True)  # 当前数据的创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 当前数据的修改时间
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinResponseContentReport(models.Model):  # 计划和结果报表主表
    plan_id = models.AutoField(primary_key=True, null=False, db_index=True, max_length=200)  # 主键
    plan_name = models.TextField(default='')  # 计划名称
    store_test_case = models.TextField(default='')  # 存入的测试用例
    execution_state = models.TextField(default='')  # 执行状态
    execution_progress = models.TextField(default='')  # 执行进度
    successs_case = models.TextField(default='')  # 成功用例
    fail_case = models.TextField(default='')  # 失败用例
    skip_case = models.TextField(default='')  # 跳过用例
    case_total = models.TextField(default='')  # 执行总数用例
    creat_time = models.DateTimeField(auto_now_add=True)  # 当前数据的创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 当前数据的修改时间
    timing_open_close = models.TextField(default='')  # 是否开启定时任务
    timing_task = models.TextField(default='')  # 定时任务
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinResponseReportResult(models.Model):
    id = models.AutoField(primary_key=True, null=False, db_index=True)  # 主键
    plan_id = models.CharField(null=False, db_index=True, max_length=200)
    plan_name = models.TextField(default='')  # 计划名称
    successs_case = models.TextField(default='')  # 成功用例
    fail_case = models.TextField(default='')  # 失败用例
    skip_case = models.TextField(default='')  # 跳过用例
    case_total = models.TextField(default='')  # 执行总数用例
    creat_time = models.DateTimeField(auto_now_add=True)  # 当前数据的创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 当前数据的修改时间
    timing_open_close = models.TextField(default='')  # 是否开启定时任务
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestDatabase(models.Model):  # 连接测试数据库
    id = models.AutoField(primary_key=True, null=False, db_index=True)
    host = models.CharField(max_length=60)
    username = models.CharField(max_length=60)
    password = models.CharField(max_length=60)
    port = models.CharField(max_length=60)
    remark = models.CharField(max_length=60, default='')  # 备注说明


class DolphinTestEmail(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    host = models.TextField(default='')  # SMTP服务器:smtp.163.com
    port = models.TextField(default='')  # 端口号465
    user = models.TextField(default='')  # 用户名
    passWD = models.TextField(default='')  # 授权密码，非登录密码
    sender = models.TextField(default='')  # 发邮件人
    receivers = models.TextField(default='')  # 收邮件人
    remark = models.TextField(default='')  # 备注说明


class DolphinUser(models.Model):  # 用户表
    user_type_choices = ((1, '一级权限'), (2, '二级权限'), (3, '三级权限'))
    # username = models.CharField(max_length=50, unique=True)
    # password = models.CharField(max_length=100)
    user_id = models.AutoField(primary_key=True, null=False,
                               db_index=True)  # primary_key:主键 ;autoincrement:自动增量;NUll:是否可以为空
    user_name = models.CharField(max_length=60, db_index=True)  # 用户姓名    CharField：字符串类型
    user_account = models.CharField(max_length=60)  # 用户登录账号
    user_password = models.CharField(max_length=100)  # 用户登录密码
    creat_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    updata_time = models.DateTimeField(auto_now=True)  # 修改时间
    user_type = models.IntegerField(choices=user_type_choices, default=1)  # 用户类型
    remark = models.CharField(max_length=60, default='')  # 备注说明
    token = models.CharField(max_length=64, null=True)


class DolphinTestHeards(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    tpi_id = models.TextField(default='')  # 关联公共接口ID
    headrsName = models.TextField()  # 头部名称
    headrsValues = models.TextField()  # 值
