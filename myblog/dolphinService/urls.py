"""EasyTest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from myblog.dolphinService.dolphinUser import  dolphinServeiceUserlogin
from  myblog.dolphinService.dolphinProject import dolphinProject,dolphinGroup
from myblog.dolphinService.dolphinReport import dolphinReportAction
from myblog.dolphinService.dolphinSystem import dolphinTestEmail, dolphintestPersonnel
from myblog.dolphinService.dolphinInterfaceManagementList import dolphinIntereface, dolphinTestCaseAction, \
    dolphinPubInter
from  myblog.dolphinService.dolphinJOBService import upload
from myblog.dolphinService.dolphinTestPlan import dolphinTestPlanAction
from django.conf.urls import url

urlpatterns = [
    url(r'login$', dolphinServeiceUserlogin.AuthView.as_view(), ),

    url(r'addProject', dolphinProject.addProject, ),#Project#项目接口
    url(r'getProject', dolphinProject.getProject, ),

    url(r'addGroup', dolphinGroup.addGroup, ),#Group项目分组接口
    url(r'getGroup', dolphinGroup.getGroup, ),
    url(r'updateGroup', dolphinGroup.updateGroup, ),
    url(r'deleteGroup', dolphinGroup.deleteGroup, ),


    url(r'addInterface', dolphinIntereface.addInterface, ),#Intereface接口管理
    url(r'getInterface', dolphinIntereface.getInterface, ),
    url(r'updateInterface', dolphinIntereface.updateInterface, ),
    url(r'deleteInterface', dolphinIntereface.deleteInterface, ),
    url(r'getRequest', dolphinIntereface.getRequest, ),

    url(r'addCase', dolphinTestCaseAction.addCase, ),  # 添加测试用例
    url(r'getCase', dolphinTestCaseAction.getCase, ),
    url(r'getOneCase', dolphinTestCaseAction.getOneCase, ),
    url(r'deleteCase', dolphinTestCaseAction.deleteCase, ),
    url(r'getAllCass', dolphinTestCaseAction.getAllCass, ),
    url(r'copyCase', dolphinTestCaseAction.copyCase, ),

    url(r'checkPoint', dolphinTestCaseAction.checkPoint, ),  # 检查点
    url(r'checkCase', dolphinTestCaseAction.checkCase, ),  # 检查用例
    url(r'executionCase', dolphinTestCaseAction.executionCase, ),

    url(r'getTestDB', dolphinTestCaseAction.getTestDB, ),  # 获取测试库
    url(r'getTestSQL', dolphinTestCaseAction.getTestSQL, ),  # 获取测试库
    url(r'getHerders', dolphinTestCaseAction.getHerders, ),  # 获取头部信息库

    url(r'AssertionCase', dolphinTestCaseAction.AssertionCase, ),  # 获取头部信息库

    url(r'gettingUseCases', dolphinTestPlanAction.gettingUseCases, ),  # 获得多级联动筛选条件
    url(r'getTestPlanCases', dolphinTestPlanAction.getTestPlanCases, ),  # 获得添加测试用例计划列表
    url(r'addTestPlan', dolphinTestPlanAction.addTestPlan, ),  # 添加测试计划
    url(r'queryTestPlan', dolphinTestPlanAction.queryTestPlan, ),  # 查询测试计划
    url(r'deleteTestPlan', dolphinTestPlanAction.deleteTestPlan, ),  # 删除测试计划

    url(r'executetTestPlan', dolphinTestPlanAction.executetTestPlan, ),  # 删除测试计划

    url(r'addPublicInterface', dolphinPubInter.addPublicInterface, ),  # 新增公共测试计划
    url(r'queryPublicInterface', dolphinPubInter.queryPublicInterface, ),  # 查询公共测试计划
    url(r'runNerPublicInterface', dolphinPubInter.runNerPublicInterface, ),  # 运行公共测试计划
    url(r'delPublicInterface', dolphinPubInter.delPublicInterface, ),  # 删除公共测试计划

    url(r'getReportPlan', dolphinReportAction.getReportPlan, ),  # 获取报告条数
    url(r'getReportCase', dolphinReportAction.getReportCase, ),  # 获取报告信息

    url(r'getEmail', dolphinTestEmail.getEmail, ),  # 获得邮件
    url(r'addEmail', dolphinTestEmail.addEmail, ),  # 增加修改邮件
    url(r'testEmail', dolphinTestEmail.testEmail, ),  # 测试邮件

    url(r'adduserPresonnel', dolphintestPersonnel.adduserPresonnel, ),  # 新增修改用户接口
    url(r'getPresonnel', dolphintestPersonnel.getPresonnel, ),  # 获取单条接口
    url(r'getALLPresonnel', dolphintestPersonnel.getALLPresonnel, ),  # 获得所有用户
    url(r'delPresonnel', dolphintestPersonnel.delPresonnel, ),  # 获得所有用户





    url(r'uploadtoken', upload.get_token, ),#七牛云

]