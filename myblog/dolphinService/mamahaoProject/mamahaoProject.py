default_encoding = 'utf-8'
# coding: utf-8
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from rest_framework.decorators import api_view
import json
from myblog.models import DolphinTestProject
from myblog.dolphinService.baseCode import getResonCode
from django.core import serializers
import logging
logger = logging.getLogger('log')

@csrf_exempt  # 允许跨域访问
@api_view(['POST'])
def addProject(request):#新增项目
    data = eval(json.loads(request.body)["data"])
    try:
        if (data["name"] != None and data["runningAddress"] !=None):
            project = DolphinTestProject(pj_img=data["imageUrl"], pj_name=data["name"],
                                         pj_running_address=data["runningAddress"], remark=data["describe"],
                                         configure_address=data["configure_address"],
                                         otherconfigure=data["otherconfigure"]
                                         )
            project.save()
            response = getResonCode().getSuccess()
            return JsonResponse(response)
        else:
            response = getResonCode().getFail()
            response["msg"] = "缺少参数"
            return JsonResponse(response)
    except EOFError as e:
        logging.error(e)
        response = getResonCode().getErorr()
        return JsonResponse(response)

@csrf_exempt
@api_view(['GET'])
def getProject(request): #获取查询项目表
    try:
        a=DolphinTestProject.objects.all()
        data = json.loads(serializers.serialize("json", a))
        response = getResonCode().getSuccess()
        response["data"]=data
        return JsonResponse(response)
    except EOFError as e:
        logging.error(e)
        response = getResonCode().getErorr()
        return JsonResponse(response)





