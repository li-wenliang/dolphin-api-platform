# -*- coding:utf-8 -*-
"""
该@shared_task装饰可以让你创建任务，而无需任何具体的celery实例
其使用的celery实例为当前活跃的celery
"""
from __future__ import absolute_import
from celery import shared_task
from myblog.dolphinJOB.AsynchronousTaskExecution import dolphinTask
from myblog.dolphinJOB.send_mail import send_email1

import time
import logging
logger = logging.getLogger('log')

@shared_task
def ExecuteTestPlanAsynchronously(id):
    try:
        logger.info("--------执行异步测试计划开始---------------")
        dolphinTask().starPublicInter()  # 执行更新所有头部信息
        dolphinTask().testBeingPlan(id=id)  # 执行中
        dolphinTask().testExecutePlanAsynchronously(id=id)  # 去执行

        dolphinTask().testOverPlan(id=id)  # 执行结束
        logger.info("--------发送邮件---------------")
        data1 = dolphinTask().testSandEmail(id=id)
        send_email1(data=data1)
        logger.info("--------执行异步测试计划结束---------------")
    except Exception as e:
        dolphinTask().errorTestPlan(id=id)
        logger.error(e)
    return None


@shared_task
def specifyScheduledTasks(a):
    logger.info("执行----定时任务开始------------")
    from django.utils import timezone
    print(timezone.now, a)
    time.sleep(5)
    logger.info("执行----定时任务结束----------")

    return 1


@shared_task
def xsum():
    print("xsum任务开始-----")
    time.sleep(1)
    print("xsum任务结束-----")
    return 1
