---



---

# 自动化接口测试平台从零实战

## 前言

本文介绍项目从0开始学习Python测试自动化平台的构成和讲解

为什么要做测试平台化的管理？需要解决什么？我们能做什么？

市面上主流的自动化软件测试主流框架：

接口自动化 ：request+nose（或者unittest）  

 UI   自动化 ：selenium

APP自动化  ：appnium

上述框架为了用例的高维护性，便捷性和会采用相关表格存储数据,使用数据驱动或者页面工厂模式等等设计模式进行优化框架来达到目的，但是这种类型的维护性就好比你在手动维护数据库。需要修改代码修改存储表格，即便特别优秀的设计模式，维护上限并不高。所以用到可视化界面去维护，即便不懂代码的测试人员也依然能够操作使用，可维护性又是一个质的提高，并且时间成本和人力成本都会进行下降。

目前来说所需要自动化的目的：1.回归测试减少时间和人力成本，2.增加对软件的信心，3.衡量质量的标准之一。特别是针对敏捷式并行开发模式，在A分支的A1时间点上合并至master，那么B分支在B1时间点上合并master,就会产生在B1时间上很难预测A的功能性是否受到影响，这是开发项目模式决定的，为了保证质量和整体项目的可靠性是需要回归自动化测试，但是由于创业型公司或者一般10人以下测试团队，很少有专门的测试开发或者业务上的快速迭代很难去做自动化的用例管理，为了解决用例的管理和尽可能多的其他功能型测试也能参与进来维护，减少成本的同时提高效率,所以进行平台化可视化模式进行开发测试自动化。

当前平台采用市场主流的前后端分离模式 这样便于后续维护性和方便有人能接盘你的东西 无论是测试者还是开发者都是主流的框架 方便其他人修改。

**后端：Django REST Framework**

​	django是一个由python编写的具有完整架站能力的开源Web框架。也属于快速上手和快速开发的框架，灵活性很高还有强大的数据库访问组件，自带数据库orm组件。学习文档也很完善

django rest framework可以在django的基础上迅速实现API。

**前端：vue + element UI**

​	vue的数据双向绑定以及设计模式比较好用 而且初级上手不算特别难所以选择他。 element ui能够快速搭建网站样式，文档很完善 vue主流框架用的人多。遇到问题方便解决，包括后续打包集成等都是很方便的。

**数据库：mysql**

​	常用数据库，大多数人都熟悉的数据库

**持续化集成：jenkins**

​	网站的高效发布迭代必备的神器了。

**中间件：redis**

​	这里redis用作消息队列。当然你也可以选择用MQ，这里我比较熟悉redis而已

## Linux安装部署到持续化集成

### 环境安装和相关依赖：

#### Python3.7 

##### python依赖包

```python
amqp==2.5.2
celery==4.4.0
certifi==2019.3.9
Django==2.0.1
django-celery-beat==1.6.0
django-cors-headers==3.0.2
django-timezone-field==3.1
djangorestframework==3.10.2
flower==0.9.3
idna==2.8
importlib-metadata==1.4.0
kombu==4.6.7
lxml==4.4.1
more-itertools==8.1.0
PyMySQL==0.9.3
python-crontab==2.4.0
pytz==2019.1
qiniu==7.2.6
redis==3.3.11
requests==2.21.0
six==1.14.0
sqlparse==0.3.0
tornado==5.1.1
urllib3==1.24.1
vine==1.3.0
xlrd==1.2.0
xlutils==2.0.0
xlwt==1.3.0
zipp==1.0.0
```



##### python3.7安装下载教程

```shell
#在安装python3前，我们需要安装一些必要的依赖包，直接通过 yum 命令安装即可
yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel
#3.7版本需要一个新的包libffi-devel
yum install libffi-devel -y
#新建存储下载包等文件夹
mkdir -p /usr/python3
#进入新建的文件夹
cd /usr/python3
#下载python3.7包
wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
#解压python包
tar -xvf Python-3.7.0.tgz
#进入解压python文件夹
cd Python-3.7.0
#编译安装。
./configure --prefix=/usr/local/python3  
make && make install
#建立软连接
ln -s /usr/local/python3/bin/python3.7  /usr/bin/python3
ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
(删除软连接： rm -rf /usr/bin/python3)
#升级PIP3
pip3 install --upgrade pip

```



#### Redis

##### redis 安装下载教程

```shell
#进入到之前创建的python3文件家中
cd /usr/python3
#下载redis包
wget http://download.redis.io/releases/redis-3.2.1.tar.gz
#解压redis包
tar xzvf redis-3.2.1.tar.gz
#进入到redis文件夹中
cd redis-3.2.1
#编译
make
#进入到redis src目录中
cd src
#安装到指定目录
make install PREFIX=/usr/local/redis
#移动配置文件到安装目录下
cd ../
mkdir /usr/local/redis/etc
mv redis.conf /usr/local/redis/etc
#建立软连接（可以直接用redis-cli命令进入到客户端内）
ln -s /usr/python3/redis-3.2.1/src/redis-cli   /usr/bin/redis-cli
ln -s /usr/local/redis/bin/redis-server  /usr/bin/redis-server
#修改redis配置 
vim /usr/local/redis/etc/redis.conf
#寻找并进行修改内容如下↓
修改第一处：daemonize no >>> daemonize yes（后端模式启动）
修改第二处：bind 127.0.0.1 >>> bind 0.0.0.0（外网访问）（记得取消注释）
#启动ridis
redis-server /usr/local/redis/etc/redis.conf
#关闭ridis
pkill redis(redis-cli -p 6379 shutdown)
#本次安装不需要设置的密码 只是用作消息队列

# 卸载redis：
rm -rf /usr/local/redis #删除安装目录
rm -rf /usr/bin/redis-* #删除所有redis相关命令脚本
m -rf /usr/python3/redis-*#删除redis解压文件夹

#开启远程访问redis
vim /usr/local/redis/etc/redis.conf
修改：protected-mode yes >>> protected-mode no

```



#### Mysql

##### mysql安装下载教程

```shell
# 安装MySQL的yum源，下面是RHEL6系列的下载地址
rpm -Uvh http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm
# 安装yum-config-manager
yum install yum-utils -y
# 禁用MySQL5.6的源
yum-config-manager --disable mysql56-community
# 启用MySQL5.7的源
yum-config-manager --enable mysql57-community-dmr
# 用下面的命令查看是否配置正确
yum repolist enabled | grep mysql
(正确的时候长下面这样↓)
#mysql-connectors-community/x86_64 MySQL Connectors Community                 121
#mysql-tools-community/x86_64      MySQL Tools Community                       87
#mysql57-community-dmr/x86_64      MySQL 5.7 Community Server Development     378
#修改yum源文件地址
vim /etc/yum.repos.d/mysql-community.repo

[mysql57-community]
name=MySQL 5.7 Community Server
## baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/6/$basearch/   
baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/7/$basearch/

#开始安装
yum install mysql-community-server
#首先启动MySQL
systemctl start  mysqld.service
#查看MySQL运行状态
systemctl status mysqld.service
#找到密码
grep "password" /var/log/mysqld.log
---A temporary password is generated for root@localhost: 3egYm;<wj&jE--
---"3egYm;<wj&jE"这个就是密码---
#进入mysql
mysql -uroot -p (回车后)
--输入找到的密码--
#修改密码（先修改一个复杂的）
mysql>ALTER USER 'root'@'localhost' IDENTIFIED BY '4adYm;>qd&jb';
#修改密码规则
mysql> set global validate_password_policy=0;
mysql> set global validate_password_length=1;
#修改简单的密码
mysql>ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';
#可视化的客户端进行连接，需要我们进行授权：
mysql> grant all on *.* to root@'%' identified by '123456';
#因为安装了Yum Repository，以后每次yum操作都会自动更新，需要把这个卸载掉：
yum -y remove mysql57-community-release-el7-10.noarch

```



#### Jenkins

##### jenkins安装下载教程

```shell
#jenkins的安装有很多种，这里选择相对简单的，只需要执行命令就行
#安装JAVA环境JDK
yum install -y java
#如果不能安装就到官网下载jenkis的rmp包 就执行下面的
1 wget http://pkg.jenkins-ci.org/redhat-stable/jenkins-2.222.1-1.1.noarch.rpm
2 rpm -ivh jenkins-2.222.1-1.1.noarch.rpm

#配置jenkis的端口
vi /etc/sysconfig/jenkins
找到内容：JENKINS_PORT="8080"  (此端口不冲突可以不修改) 
#Jenkins启动/停止/重启 
service jenkins start
service jenkins stop
service jenkins restart
#启动后成功后输入密码，寻找密码
cat /var/lib/jenkins/secrets/initialAdminPassword
#拷贝密码输入进去就可以了 

```

##### jenkins常用配置插件

|               插件名称               |                             作用                             |
| :----------------------------------: | :----------------------------------------------------------: |
|           Publish Over SSH           |   通过SSH拷贝文件到目标机器，同时可以在目标机器上执行脚本    |
|              Monitoring              |    监控Jenkins节点的CPU、系统负载、平均响应时间和内存使用    |
|              Git plugin              |                      通过版本库获取代码                      |
|        Email Extension Plugin        |          用于替换Jenkins自带的邮件发送，更加的强大           |
|        Git Parameter Plug-In         |                       获取GIT分支插件                        |
|        Active Choices Plug-In        |                       选择指定的IP发布                       |
|  Role-based Authorization Strategy   |             给Jenkins用户权限管理添加了角色组。              |
| Matrix Authorization Strategy Plugin |                    为每个项目设置用户权限                    |
|                Sounds                |          这个插件能让Jenkins通过播放声音来发出通知           |
|                Python                |      这个插件支持在Jenkins的构建过程中执行Python脚本。       |
|            Pyenv Pipeline            | 方便对python进行项目级别的环境隔离。<br/>jenkins机器上需要安装python、pip、virtualenv |
|                Maven                 |          为Maven 2 / 3项目提供了高级集成功能。Java           |
|          SSH Slaves plugin           |                  可直接在远端使用shell脚本                   |



#### Nginx

##### nginx安装下载教程

```shell
#一键安装上面四个依赖
yum -y install gcc zlib zlib-devel pcre-devel openssl openssl-devel
#进入python3文件夹中
cd /usr/python3
#获取nginx的下载链接
wget http://nginx.org/download/nginx-1.14.0.tar.gz
#解压并进入文件夹
tar -xvf nginx-1.14.0.tar.gz	
cd nginx-1.14.0
#配置并安装编译
./configure
make & make install
#进入配置文件
vim /usr/local/nginx/conf/nginx.conf	
#启动nginx
/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
#重启nginx配置文件
/usr/local/nginx/sbin/nginx -s reload
```



#### Node

##### vue安装下载教程

```shell
#进入python3文件夹中
cd /usr/python3
#下载node链接
wget https://nodejs.org/dist/v11.10.0/node-v11.10.0-linux-x64.tar.xz
#解压node
tar xvf node-v11.10.0-linux-x64.tar.xz
#添加软连接
ln -s /usr/python3/node-v11.10.0-linux-x64/bin/node  /usr/bin/node
ln -s /usr/python3/node-v11.10.0-linux-x64/bin/npm  /usr/bin/npm
#查看是否安装成功
node -v  
npm -v
#安装淘宝镜像
npm install -g cnpm --registry=https://registry.npm.taobao.org
#添加cnpm软连接
ln -s /usr/python3/node-v11.10.0-linux-x64/bin/cnpm  /usr/bin/cnpm
#安装vue-cli脚手架
cnpm install -g vue-cli

```



### 持续集成部署和启动

#### 环境准备

```shell
#顺便安装一下这个lsof命令
yum install lsof
#将Python依赖包放入名字叫做requirements.txt的文件中 安装pip第三方库
pip3 install -r requirements.txt
#升级celery  async关键词冲突等相关版本问题（先安装python依赖包后在执行下面的命令）
pip3 install --upgrade https://github.com/celery/celery/tarball/master
#建立celery软连接
ln -s /usr/local/python3/bin/celery  /usr/bin/celery

```

到这里环境的准备就告一段落了。

接下来就是 Jenkins 的部署，

#### 持续集成 

一般Jenkins部署有两种 一种在拉下来代码在本地直接启动，我们这里讲在远端服务打包到测试平台环境的。因为大多数公司都有自己的测试环境Jenkins 所以我们一般很少自己装。

我们只需要寻问运维或者管理服务器的人要一台服务器将上面的所需要的环境装好然后在测试Jenkins上面直接开始即可。

以下开始模拟远端发包全过程

##### 1，下载插件

我们在上面已经说过了常用的插件，下载后读取配置即可

##### 2，工程搭建

###### 服务端构建

新建项目名称为：“test_Django”

构建自由风格类型项目点击“确定”按钮

![image-20200424100111920](http://chq.lihail.cn/image-20200424100111920.png)



因为我当前项目我自己公司和线上是两个环境，在配置文件上面会有会有所不同，所以我会新建一个分支来进行部署线上的环境。

**1，先进行描述一下，不然团队的人不知道这是什么。**

![image-20200424100146392](http://chq.lihail.cn/image-20200424100146392.png)



**2，保留构建的天数和次数的最大值设置** 

![image-20200424100206487](http://chq.lihail.cn/image-20200424100206487.png)

**3，勾选参数化构建过程选择参数**

![image-20200424135047781](http://chq.lihail.cn/20200424135051.png)

**4，GIT参数配置**

version获取分支变量名在拉取代码中会有显示

![image-20200424135507265](http://chq.lihail.cn/20200424135509.png)

![image-20200424140328070](http://chq.lihail.cn/20200424180620.png)

选择高级展开列表后，选择快速过滤进行单选框勾选，后选择列表数量 。

勾选快速过滤：可以进行筛选代码分支

列表数量：展示数量

5，**选项参数**

名称：选择IP的变量名，进行选择远程发包到哪台服务器里面去，我这里就一台 这是提供一个模拟的测试开发环境的思路（ 单纯部署当前项目的话可以忽略）

![image-20200424135758789](http://chq.lihail.cn/20200424135801.png)

**6，Active Choices**

这里是根据选“**选项参数**”选择的SERVER_IP 通过选择不同的IP去获取不同的配置文件。（ 单纯部署当前项目的话可以忽略）

![image-20200424140023038](http://chq.lihail.cn/20200424140029.png)

![image-20200424140253742](http://chq.lihail.cn/20200424180651.png)

**7，源码管理**

${version} 这里填写这个，和GIT参数配置进行传递

![image-20200424140546548](http://chq.lihail.cn/20200424140551.png)

**8，执行Shell**

${WORKSPACE} :是当前jenkins当前工程下的路径

​	因为从A-jenkins服务器打包到B启动项目的服务器上所以需要执行进行打包一次，JAVA会有jar包war包等，Python没有这种类型的 所以我这里直接打包成为压缩包等待发送到B服务器上。

${conf}:是上述供你选择Active Choices这里供你选择配置文件，我这里没有其他的配置文件所以只是供己学习一下。

![image-20200424140814511](http://chq.lihail.cn/20200424140817.png)

```shell
cd ${WORKSPACE}
rm -rf dolphinVue 
rm -rf mamhaotestDjango.tar.gz
cd ../
tar zcvf mamhaotestDjango.tar.gz test_Django
mv mamhaotestDjango.tar.gz ${WORKSPACE}
```

**9.构建后操作**

![image-20200424141729890](http://chq.lihail.cn/20200424141805.png)

![image-20200424141754796](http://chq.lihail.cn/20200424141757.png)

选择构建后操作，ssh和email两个 

email选择完就可以不用动了，因为我已经在jenkins系统配置进行全局设置了

下面是ssh发包到远端的写法，我们已经在jenkins工程里面打包好了一个压缩包，所以把打包的源文件名词放入到第一行Source files中

然后jenkins自动把源文件放入到远端服务起里面，文件地址在默认的配置全局变量里。

![image-20200424142003652](http://chq.lihail.cn/20200424142006.png)

```shell
#!/bin/bash
port="8081"
pid=$(lsof -i:$port| grep -v "COMMAND" |awk '{print $2}')
if [ -n "$pid" ]; then
kill -9 $pid
cd  /data/test/test_Django/
ls|grep -v "dolphinVue"|grep -v "celery-worker-log.log"|grep -v "celery-beat-log.log"|grep -v "celerybeat.pid"|xargs rm -rf
fi
sleep 1
cp -rf /usr/python3/file/mamhaotestDjango.tar.gz  /data/test/
cd /data/test/
tar -xvf  mamhaotestDjango.tar.gz

sleep 1
cd test_Django
rm -rf  mamhaotestDjango.tar.gz
python3 manage.py makemigrations 
python3 manage.py migrate
nohup python3 manage.py runserver 0.0.0.0:8081 --insecure>log &
```

到这里后端工程就写完了

我们需要回过头看一下 jenkins的全局变量

**10.全局设置SSH和Email**

**ssh**

![image-20200424142633508](http://chq.lihail.cn/20200424142637.png)

红框显示的-这就是默认设置的路径，测试一下是通过的

![image-20200424142731140](http://chq.lihail.cn/20200424142733.png)

**email**

![image-20200424142856728](http://chq.lihail.cn/20200424142859.png)

这里的 User Name 要和下图的jenkins管理员邮箱要是一样的

![image-20200424142950030](http://chq.lihail.cn/20200424143005.png)

然后邮件中选择 总是发送或者失败是发送都行。

![image-20200424143118494](http://chq.lihail.cn/20200424143121.png)

因为我是同一个GIT地址没有分割开来所有才会有删除前端的包这一个操作

到服务端这里就结束了。

###### 前端构建

我们这里只需要复制一下后端的构建内容，然后进行修改即可

![image-20200424143409150](http://chq.lihail.cn/20200424143439.png)

1,修改执行shell语句

![image-20200424143516711](http://chq.lihail.cn/20200424143518.png)



```shell
cd ${WORKSPACE}
tar zcvf dolphinVue.tar.gz dolphinVue
```

2,修改SSH内容

![image-20200424143621353](http://chq.lihail.cn/20200424143623.png)

```shell
#!/bin/bash
cp -rf /usr/python3/file/dolphinVue.tar.gz  /data/test/test_Django
cd /data/test/test_Django/
tar -xvf  dolphinVue.tar.gz
sleep 1
rm -rf  dolphinVue.tar.gz
cd dolphinVue
cnpm i
cnpm run build
```

前端这里就构建完成了

###### 异步服务celery构建

1，在jenkins系统设置设置全局变量

![image-20200424151122725](http://chq.lihail.cn/20200424151124.png)

2，新建jenkins工程 自由风格类型，取名“test_celery”

在构建一栏选择“execut shell script on remote host using ssh”

![image-20200424150900480](http://chq.lihail.cn/20200424150903.png)



```shell
cd /data/test/test_Django
if [ -f "celerybeat.pid" ];then
echo "celerybeat.pid存在"
rm -rf celerybeat.pid
else
echo "celerybeat.pid不存在"
fi
redispid=$(ps -ef|grep -v "grep"|grep "redis"|awk '{print $2}')
if [ -n "$redispid" ]; then
echo "redis正常运行"
else
echo "启动redis"
redis-server /usr/local/redis/etc/redis.conf //启动redis
fi
pid=$(ps -ef|grep -v "grep"|grep -v "worker"|grep celery|awk '{print $2}')
if [ -n "$pid" ]; then
kill -9 $pid
fi
#启动beat
nohup celery -A  untitled4 beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler  > celery-beat-log.log 2>&1 &

pid2=$(ps -ef|grep -v "grep"|grep -v "beat"|grep celery|awk '{print $2}')
if [ -n "$pid2" ]; then
kill -9 $pid2
fi
#启动worker
nohup celery -A untitled4  worker --pool=solo  > celery-worker-log.log 2>&1 &

```

这样就可以了直接可以启动异步服务

##### 结果展示

![image-20200424151416515](http://chq.lihail.cn/20200424151419.png)

选择分支

![image-20200424151540296](http://chq.lihail.cn/20200424151542.png)

邮件显示

![image-20200424151743242](http://chq.lihail.cn/20200424151745.png)



## 项目实操

### 登录

```mysql
mysql>CREATE DATABASE mydjango DEFAULT CHARACTER
SET utf8 COLLATE utf8_bin;
#创建数据库
```

因为刚部署上项目 没有账号需要我们手动在数据库中添加一个账号。

```mysql
INSERT INTO mydjango.myblog_dolphinuser (
	user_name,
	user_account,
	user_password,
	creat_time,
	user_type
)
VALUES
	(
		"admin",
		"超级管理员",
		"51433991b61d3324aff461c2173d669d",
		NOW(),
		"1"
	)
登录账号：admin
登录密码：123456
```

![image-20200426111234751](http://chq.lihail.cn/20200426111236.png)

### 新建项目

#### **添加项目**

![image-20200426111414695](http://chq.lihail.cn/20200426111421.png)

项目图片：不是必填项，不填写会有默认图片上去。

项目名称：必填项正常填写即可

项目描述：必填项正常描述

项目环境：必填项，提交时会有校验。

其他环境1：非必填项（多接口用例时，动态调整请求路径所用）

其他环境2：非必填项（多接口用例时，动态调整请求路径所用）

![image-20200426111612358](http://chq.lihail.cn/20200426111614.png)

#### **添加分组**

点击项目，进入分组页面

![image-20200426112416716](http://chq.lihail.cn/20200426112418.png)





![image-20200426112541800](http://chq.lihail.cn/20200426112544.png)



查看按钮：进入到分组内

编辑按钮：修改分组的信息内容

删除按钮：点击进行删除（如果分组中有接口信息时，则不允许删除当前分组数据的 必须删除当前分组里内容）

分组内容主要继续区分和管理接口用例。

#### **接口管理**

![image-20200426112821680](http://chq.lihail.cn/20200426112824.png)

分组下，作为接口管理内容进行管理接口

查看用例：进入当前接口下进行用例的管理

编辑按钮：修改当前接口的信息内容

删除按钮：如果当前接口内容里包含用例是是不允许进行删除当前接口的

添加接口内容：

接口名称：必填项目

接口地址：必填项

请求方式：目前支持get和post常用接口方式

#### **用例管理**

![image-20200426113504163](http://chq.lihail.cn/20200426113508.png)



批量执行：批量执行接口用例

新增用例：进入新增用例的页面

复制按钮：复制一个相同的用例内容

编辑按钮：进入当前用例的编辑页面

删除按钮：删除当前用例

用例执行成功时，数据状态一栏是绿色的，执行失败时红色的。

首次添加用例时则是红色失败状态。

#### **新增用例**

**基础信息**：当前用例的接口信息，携带接口信息内容 

![image-20200426113937523](http://chq.lihail.cn/20200426113942.png)

**前置条件**：头部信息内容

取值方式键：${abc} 

取值方式值：下拉框选择（为公共接口内容，需要在公共接口添加）

![image-20200426114123287](http://chq.lihail.cn/20200426114127.png)

如果是多个头部信息需要取值的话：

![image-20200426114441456](http://chq.lihail.cn/20200426114447.png)

可以使用@#$%&*特使符合 加上大括号进行取值

如图则是：${heard}，#{abc}

**请求参数设置**：用例请求

![image-20200426114934730](http://chq.lihail.cn/20200426114937.png)



用例名称：必填项

数据类型，必填项，分别是data(params)和json(body)

是否需要其他接口：是本接口的上级接口，开关打开则使用 关闭则隐藏

前置SQL：在当前用例运行前使用之前进行查询数据库。数据库为测试接口的用的库

头部信息：可以为空，也可以手动填写{“key”：“value“}的内容，也能够通过前置条件填写传参内容${heard}

入参数据：键值传参，键值可以填写正常的值，传递的值

后置SQL：在执行完用用例后执行SQL

断言数据：键,由最终接口提供的参数，比值,可以是自己填写的值，也可以时多接口传递的值，也可以是SQL返回的值。

**是否需要其他接口**-详解

1，打开按钮

![image-20200426134959614](http://chq.lihail.cn/20200426135003.png)



接口地址：可以是HTTPS和HTTP，跟随项目是请求域名为跟随当前项目，也可以自行调整 环境1和环境2，也能够自己填写接口域名。

头部信息：填入你自己写的传参${heard}

请求方式和数据类型：下拉框选择数据方式

请求入参：选择参数，也可以是多接口的关联取值，填入。

检查点：

通过检查点可以检查关联用的请求情况。

![image-20200426140543313](http://chq.lihail.cn/20200426140546.png)

我们将这个${recordCount} 放入到下一个多接口的入参中。

这里recordCount=9

![image-20200426143918676](http://chq.lihail.cn/20200426143920.png)

我们把size替换成了上个接口传递的${recordCount}=9,然后在吧${name}取第三个值。

预期结果应该是，size的大小应该是9，然后相同名字name应该取第三个。

运行一下检查点。

![image-20200426144202136](http://chq.lihail.cn/20200426144234.png)

![image-20200426144230929](http://chq.lihail.cn/20200426144239.png)

我们可以看到新增门店 name第三个 {pre门店测试新增016}和我们预计看到是一样的。接口返回参数size正好也是9。执行结果是正确的

而，提取数据：{ "${recordCount}": 9, "${name}": "pre门店测试新增016" }这个列表会一直存储在一起多接口也可以共同传参使用。

**前置SQL**

![image-20200426144548174](http://chq.lihail.cn/20200426144552.png)

![image-20200426144854019](http://chq.lihail.cn/20200426144856.png)

填写SQL后运行SQL，显示查询结果。

![image-20200426145532222](http://chq.lihail.cn/20200426145534.png)

这里数据库 取值

![image-20200426150613200](http://chq.lihail.cn/20200426150614.png)

预期${STYLE_TITLE}="成人奶粉0612"  , ${ONLINE_CLOUD}=1

**入参和检查用例**

然后我们开始讲这两个参数带过去， 

![image-20200426150023747](http://chq.lihail.cn/20200426150025.png)

点击检查用例。

查看styleTitle的传参，和page的传参是不是 "成人奶粉0612" ，“1”这两个字段

```json
 POST请求入参：{'styleTitle': '成人奶粉0612', 'dataType': '1', 'needCategory': 'true', 'page': '1', 'pageNo': '1', 'pageSize': '10', 'requestFrom': '5', 'shopId': '1500000279', 'size': '10'}
```

通过查询日志后'page': '1'，'styleTitle': '成人奶粉0612' 日志是成功显示的。

重点：入参的值，可以填入前置SQL传过来的值，也可以传入多接口传过来的值。

**后置sql**

功能使用上和前置SQL一致 只是在接口执行后进行执行。

**断言数据**

![image-20200426150806892](http://chq.lihail.cn/20200426150808.png)

左边的键，是当前最后一个接口执行的返回的参数，以及后置数据库中传入的值 通过检查用例 把需要对比的值放入到左边的键中。

右边的值，是可以对比参数可以使手动填写的值，也可以是前置数据库中传入的值，也可以是多接口传递的值。

不一致的时候：	

![image-20200426152041608](http://chq.lihail.cn/20200426152057.png)

一致的时候：

![image-20200426152906626](http://chq.lihail.cn/20200426152908.png)



最后在强调一遍 左边的是 最终用例的出参结果+后置SQL查询的结果，如果有重复的相同的字段 可以用

￥#@！+{} 比如：#{aa}，￥{aa},来进行区分相同的字段不同的值。

右边的字段，是自己填写和前置多接口的动态传参值，以及前置SQL的值进行对比的。



### 公共接口

为了获取头部信息的公共接口，如果较为复杂的头部信息接口 可有测试人员自己写工具类接口封装进来。

![image-20200426153232604](http://chq.lihail.cn/20200426153237.png)

### 计划管理

<img src="http://chq.lihail.cn/20200426153400.png" alt="image-20200426153344285" style="zoom: 150%;" />

**新增计划**

![image-20200426153453961](http://chq.lihail.cn/20200426153458.png)



可以勾选定时任务

定时任务规则如下：

1. 比如:0 */2 1-5 * *
2. 每个工作日1-5 每两个小时0分钟开始执行
3. 标准5个单位，每个单位以空格隔开
4. \* : 范围内的所有值
5. M-N : M到N之间的值
6. M-N/X 或 */X : 每X分钟、每X天等等

计划任务名称：1，不能够重复，2，必填项

添加用例：弹窗显示，通过勾选用例，来添加计划计划任务

![image-20200426153714901](http://chq.lihail.cn/20200426153716.png)

添加成功后也可以及时进行删除，和再次添加。

![image-20200426153844955](http://chq.lihail.cn/20200426153848.png)



执行计划任务。

1，数据汇总状态会进行变更 首次添加回事1尚未执行，2待执行 ，3执行中，4执行失败，5执行结束

本次为异步任务，消息队列进行排队单线程任务执行。

执行成功后会进行邮件发送

![image-20200426154257330](http://chq.lihail.cn/20200426154423.png)

### **报告管理**

用于显示报告

![image-20200426154159618](http://chq.lihail.cn/20200426154205.png)

计划分析当前近7次的所有任务的执行请看和成功率

点击数据列表可以进行切换计划任务

点击计划详情可以查看当前执行计划的详情请看：

点击详情可以直接进入用例的编辑页面

![image-20200426154416889](http://chq.lihail.cn/20200426154433.png)





### 测试工具

这里是需要专门为测试部门订制测试工具，用于提高效率 可以进多人多服务的开发。

![image-20200426154624239](http://chq.lihail.cn/20200426154627.png)

### 系统设置

**人员管理**

![image-20200426154836624](http://chq.lihail.cn/20200426154838.png)

用户的新增，用户可以进进行权限显示，超管拥有所有页面权限，普通管理这没有系统这设置页面，还有其他管理只有报告查看的页面权限。

**数据库配置**

针对对用例中去请求的数据库进行添加

**环境配置**

用于对项目的执行的环境切换

**邮件配置**

用于计划任务执行完毕后 发送任务给相关人员的配置



## setting配置文件讲解

### 配置数据库 

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': "127.0.0.1",
        'NAME': "mydjango",
        'USER': "root",
        'PASSWORD': "123456",
        'PORT': "3306",
        'CONN_MAX_AGE': 10,
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
            'charset': 'utf8mb4',
        },
    }
}
```

```python
DEBUG = False
```

如果在内网的话 是可以打开的 DEBUG = True 这样的话方便定位问题，第二就是能够帮你渲染前端，不用NGINX代理静态前端页了。会消耗点性能不过在内网服务器中并不那么重要。



